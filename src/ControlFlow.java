/**
 * Diese Klasse beinhaltet Methoden zur Berechnung unterschiedlicher Folgen mit Hilfe von Kontrollstrukturen
 *
 * @author Cedrico Knoesel
 * @since 04.12.18
 */
public class ControlFlow {

    public static void main(String[] args) {
        System.out.println(calculateSequence1(0));
        System.out.println(calculateSequence2(4));
        System.out.println(calculateSumOfInterval(45, 82));
        System.out.println(approximatePi(30) * 4);
    }

    /**
     * Berechnet die ersten 35 Folgeglieder einer Folge beginnend ab der Eingabe x1
     * Hinweis: Es wird die Kontrollstruktor for-Schleife verwendet, da eine feste Anzahl an Werten nach gleichem
     * Schema berechnet werden müssen.
     *
     * @param x1 Startwert der Folge
     * @return erste 35 Folgeglieder ab dem Startwert x1 (inklusive) durch genau ein Semikolon getrennt
     */
    public static String calculateSequence1(int x1) {
        int xn = x1;
        //Berechne die ersten 35 Folgeglieder x2, ..., x36 und schreibe sie in returnSequence
        StringBuilder returnSequence = new StringBuilder(String.valueOf(x1) + ";");
        for (int n = 2; n <= 36; n++) {
            if (xn % 12 == 0) {
                xn = (xn / 3) + 1;
            } else {
                xn = (xn * 2) + 4;
            }
            returnSequence.append(xn).append(";");
        }
        //Entferne letztes Semikolon:
        returnSequence.deleteCharAt(returnSequence.length() - 1);
        return returnSequence.toString();
    }

    /**
     * Berechnet die ersten 20 Folgeglieder einer Folge beginnend ab der Eingabe x1
     * Hinweis: Es wird die Kontrollstruktor for-Schleife verwendet, da eine feste Anzahl an Werten nach gleichem
     * Schema berechnet werden müssen.
     *
     * @param x1 Startwert der Folge
     * @return erste 20 Folgeglieder ab dem Startwert x1 (inklusive) durch genau ein Semikolon getrennt
     */
    public static String calculateSequence2(int x1) {
        int xn = x1;
        //Berechne die ersten 20 Folgeglieder x2, ..., x21 und schreibe sie in returnSequence
        StringBuilder returnSequence = new StringBuilder(String.valueOf(x1) + ";");
        for (int n = 1; n < 21; n++) {
            if (n % 2 == 0) {
                xn = -7 + (3 * n);
            } else {
                xn = 7 - (3 * n);
            }
            returnSequence.append(xn).append(";");
        }
        //Entferne letztes Semikolon:
        returnSequence.deleteCharAt(returnSequence.length() - 1);
        return  returnSequence.toString();
    }

    /**
     * berechnet die Summer aller positiven Integer Zahlen im abgeschlossenen Intervall [x1, x2]
     * Hinweis: Es wurde eine While-Schleife verwendet, da die Anzahl der Summierungen nicht konstant, sondern
     * abhängig von der Eingabe ist.
     *
     * @param x1 Start des Intervalls
     * @param x2 Ende des Intervalls
     * @return Summe der Zahlen im Intervall [x1, x2]
     */
    public static int calculateSumOfInterval(int x1, int x2) {
        int sum = 0;
        while (x1 <= x2) {
            sum += x1;
            x1++;
        }
        return sum;
    }

    /**
     * aproximiert die Kreiszahl Pi nach der Methode von Gottfried Wilhelm Leibniz
     * Hinweis: Es wurde eine for-Schleife verwendet, da zur Berechnung der Summe die Indexwerte verwendet werden.
     *
     * @param n Anzahl der Approximationsdurchläufe
     * @return Näherung für übergebenes n
     */
    public static double approximatePi(int n) {
        double sum = 0.0;
        if (n < 0) {
            return sum;
        }
        for (int i = 0; i <= n; i++) {
            sum += Math.pow(-1.0, i) / ((2 * i) + 1);
        }
        return sum;
    }
}
